// import logo from './logo.svg';
import { useEffect, useState } from 'react';
import './App.css';
import {BrowserRouter as Router, Route, useHistory,Link  } from "react-router-dom";
import FormLogin from './dashboard/formlogin'
import FormRegister from './dashboard/formregister'
import Dashboard from './dashboard/formproducts'
import Cart from './dashboard/formcart'
import Button from '@material-ui/core/Button'
import Headers from './dashboard/header'
import FormCart from './dashboard/formcart'

function App() {
  const ClickLogout = () => {
    localStorage.removeItem('User');
  }
  return (
    <div className="App">
       <Headers />
       <Router >
          <Route exact path="/" >
             <h1>Bai Tap Dau Tien</h1>
             <FormLogin />
              <Link to="/register">
                <Button variant="contained" color="primary">
                Register!
                </Button>
              </Link>
          </Route>
          <Route exact path="/register">
            <FormRegister />
              <Link to="/">
                <Button variant="contained" color="primary">
                Đến trang Login!
                </Button>
              </Link>
          </Route>
          <Route exact path="/products" >
             <Dashboard />
             <Link to="/">
                <Button onClick={ClickLogout} variant="contained" color="primary">
                Logout!
                </Button>
              </Link>
              <Link to="/cartdetail">
                <Button variant="outlined" color="primary">
                CartDetail
                </Button>
              </Link>
          </Route>
          <Route exact path="/cartdetail" >
          <FormCart />
             <Link to="/products">
                <Button variant="contained" color="primary">
                Home!
                </Button>
              </Link>
          </Route>            
       </Router>
    </div>
  );
}
export default App;
