import React from 'react';
import { useEffect, useState } from 'react';
import {Input,TextField} from '@material-ui/core';

const Login = props => {
    const {getValue,name,label,...other} = props;
    const [value, setValue] = useState('');
    function handleValue(e){
        setValue(e.target.value);
        getValue(e.target.value,name);
    }
    return (
        <>
        <div>
            <label>{label}</label>
            <TextField onChange={handleValue} {...other}
                error
                id="outlined-error"
                variant="outlined"
                // helperText={span}
            />
            {/* <Input  onChange={handleValue} {...other} 
                error
                id="outlined-error"
                label={span} 
                variant="outlined">
            </Input> */}
        </div>
        </>
    )
}
export default Login

