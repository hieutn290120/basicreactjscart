import React from 'react';
import { useEffect, useState } from 'react';
import { Input, TextField } from '@material-ui/core';


const Person = person => {
    const [value, setValue] = useState('');
    const { getValue, name, label, span, ...other } = person;
    function handleValue(e) {
        setValue(e.target.value);
        getValue(e.target.value, name);
    }
    return (
        <>
            <div>
                <label>{label}</label>
                <TextField onChange={handleValue} {...other}
                    error
                    id="outlined-error"
                    variant="outlined"
                    helperText={span}
                />
            </div>
            {/* style={{...{height:'50px'},...style}} */}
        </>
    )
}
export default Person

