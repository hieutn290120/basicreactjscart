import React from 'react'
import Cart from '../cart/cart'

function FormCart(){
    const myObj = JSON.parse(localStorage.getItem('Cart'));
    let html = [];
    for (const value in myObj){
       html.push(<Cart {...myObj[value]}/>)
    }

    return (
        <div className="App">
            {html}
        </div>
    )
}

export default FormCart